import numpy as np
import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import skimage
import skimage.io
import skimage.measure

# Loading Dataset
fashion_mnist = tf.keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

class_names = ['Trouser', 'Ankle boot']

ind=np.argsort(train_labels)
train_images_sorted=train_images[ind]
train_images_sorted = train_images_sorted.transpose(1,2,0).reshape(-1, train_images_sorted.shape[0])

train_labels = np.sort(train_labels, axis=0)

tIndexStart = 0
tIndexEnd = 0
abIndexStart = 0

# get trousers and ankle boots
for i in range(len(train_labels)):
    if train_labels[i] == 0:
        tIndexStart = i+1
    if train_labels[i] == 1:
        tIndexEnd = i+1
    if train_labels[i] == 8:
        abIndexStart = i+1

# 20 trousers, 20 ankle boots for training
trousers_trn = train_images_sorted[:,tIndexStart:tIndexEnd-5980]
ankleboots_trn = train_images_sorted[:,abIndexStart:len(train_labels)-5980]
T_trn = np.append(trousers_trn, ankleboots_trn, axis=1)
t_trn = np.append(np.zeros(20) ,np.ones(20))
t_trn = t_trn.astype(int)

# This method visualizes M images stored as column vectors.
def display_column_vectors (M, title=None, columns=10):
    """Dsiplays images stored as column vectors.

    Args:
        M (array)     : n_pixels x n_objects matrix storing the images in columns
        title (str)   : title of the plot
        columns (int) : how many columns in image
    """
    
    plt.figure (figsize=(8,5))
    for i, image in enumerate(M.T):
        plt.subplot(len(M.T) // columns + 1, columns, 1 + i)
        plt.imshow(image.reshape (28,28), cmap='gray')
        plt.xticks([])
        plt.yticks([])

    if title is not None:
        plt.suptitle (title)
    plt.show()

display_column_vectors(T_trn, 'Training set')

# features
features = pd.DataFrame()

for i, image in enumerate (T_trn.T):

    image = image.reshape(28,28)

    region = skimage.measure.regionprops (image) [0]  # the only connected region indexed by 0
    features = features.append (
        {     
            'class_name'   : class_names [t_trn[i]],
            'image_number'   : i,
            
            'filledArea'   : region.filled_area,
            'solidity'   : region.solidity,
            'eccentricity'   : region.eccentricity,
            'perimeter'   : region.perimeter,
            'convexArea'   : region.convex_area
            
        },
        ignore_index = True
    )

features = features.set_index ('image_number')

# 1.1.2 Perceptron training algorithm 
def percTrain (X, t, maxIts=50, online=False):
    """
    Trains a perceptron on the given training data and returns the weight vector. Please note that the first component
    of the weight vector should be the homogeneous coordinate.
    X: Input vectors
    t: target values
    maxIts: Maximum number of iterations
    online: true if online optimization procedure should be used, false for batch-version
    """
    
    
    # augment X
    x_au = np.ones((X[:,0].size+1,X[0,:].size))
    for i in range(1, x_au[:,0].size):
        x_au[i] = X[i-1]

    w = np.zeros(X[:,0].size+1).T
    epoch = 0
    if(online):
        for e in range (1, maxIts-1):
            countEpoch = True
            for i in range(0,x_au[0,:].size):
                y_hat = np.sign(np.dot(w, x_au[:,i]))
                loss = max(0,-t[i]*(np.dot(w, x_au[:,i])))
                if(loss>0): 
                    w = w+(t[i]-y_hat)*x_au[:,i]
                    countEpoch = False
            if(countEpoch):
                epoch = e
                break
    else:
        for e in range (1, maxIts-1):
            countEpoch = True
            for i in range(0,x_au[0,:].size):
                if(np.dot(w, x_au[:,i]*t[i]) <= 0):
                    w = w+x_au[:,i]*t[i]
                    countEpoch = False
            if(countEpoch):
                epoch = e
                break
                
    return w

def perc(w, X):
    """
    Use the perceptron weight vector w to predict labels on the provided data X.
    Returns a numpy array with the predictions (1 or -1) for each data item.
    X: input data
    w: perceptron weight vector, as produced by the perceptron function
    """
    predictions = np.ones(X[0].size)
    s = X.shape
    x_au = np.ones((s[0]+1,s[1]))
    
    for i in range(1, x_au[:,0].size):
        x_au[i] = X[i-1]
            
    decisions = w.T@x_au
        
    for a in range(0, decisions.size):
        if(decisions[a]<0):
            predictions[a] = -1
        
    return predictions

def plotFeatures(w, F):
    """
    Plot features using perceptron weight vector w.
    w: perceptron weight vector, as produced by the perceptron function
    F: selected features of input data

    Hint: Sample  the  relevant  region  of  the  inputspace using a meshgrid.  Compute y=wTΦ(x) for all grid points and use a 
    contour-function or a surface-plot to visualize the approximation of the curve y= 0.
    """

    x0 = F[:,0]
    x1 = F[:,1]

    #plt.scatter (x0, x1, c= ['r' if c == 1 else 'b' for c in t_trn_class])


    X0, X1 = np.meshgrid(x0, x1)
    Y = np.sqrt(np.square(X0) + np.square(X1))
    cm = plt.cm.get_cmap('viridis')
    plt.scatter(X0, X1, c=Y, cmap=cm)
    cp = plt.contour(X0, X1, Y)
    plt.clabel(cp, inline=1, fontsize=10)
    plt.xlabel('X1')
    plt.ylabel('X2')
    plt.show()
    # y = x_range*-w[1]/w[2] - w[0]/w[2]

    # plt.plot(x_range, y,linestyle = '-', c='green')
    # plt.show()

def evaluate(Y_target, Y_predicted):
    """
    Calculates the accuracy (the percentage of samples being classified correctly) as a number between 0 and 1
    and returns it.
    Y_target: List of ground truth labels
    Y_predicted: List of predicted labels, as returned by predict
    """
    acc = (Y_predicted == Y_target).mean()
    return acc

def featureTransformation (x):
    """
    TODO: change the text
    Calculates the accuracy (the percentage of samples being classified correctly) as a number between 0 and 1
    and returns it.
    Y_target: List of ground truth labels
    Y_predicted: List of predicted labels, as returned by predict
    Φ(x)  :  (x1,x2)→(1,x1,x2,x21,x22,x1x2)
    """
    features_transformed = np.ones((6,x.shape[1]))
    features_transformed[1] = features_transformed[1]*x[0]
    features_transformed[2] = features_transformed[2]*x[1]
    features_transformed[3] = features_transformed[1]*features_transformed[1]
    features_transformed[4] = features_transformed[2]*features_transformed[2]
    features_transformed[5] = features_transformed[1]*features_transformed[2]
    return features_transformed

# transform 0,1 classes to -1,1 classes
t_trn_class = 2 * t_trn - 1

features_per_ecc = np.array(features[["perimeter", "eccentricity"]]).T
features_per_ecc_t = featureTransformation(features_per_ecc)
w2ft = percTrain (features_per_ecc_t,t_trn_class, 50, True)
print("w online:  " ,w2ft)
w2ft = percTrain (features_per_ecc_t,t_trn_class)
print("w offline:  " ,w2ft)
y2ft = perc(w2ft,features_per_ecc_t)

features_all = np.array(features[["filledArea", "convexArea", "solidity", "perimeter", "eccentricity"]]).T
w5ft = percTrain (features_all,t_trn_class)

w_trn = percTrain (T_trn,t_trn_class)
#plotFeatures(w_trn, T_trn)
y_trn = perc(w_trn,T_trn)

plt.imshow(w_trn[1:].reshape (28,28), cmap='gray')
plt.show()


print("Accuracy for 2 features (perimeter and eccentricity) " ,evaluate(t_trn_class, y2ft))
plotFeatures(w2ft, features_per_ecc_t)
y5ft = perc(w5ft, features_all)
print("Accuracy for 5 features " ,evaluate(t_trn_class, y5ft))

print("Accuracy for whole images " ,evaluate(t_trn_class, y_trn))