import numpy as np
import random
import matplotlib.pyplot as plt
import time

# %% training data
x = np.linspace(0., 5., 51)
G = 16
y = 2 * (x ** 2) - G * x + 1
D = 2
x_train = x[np.arange(0, len(x), 8)]
y_train = y[np.arange(0, len(x), 8)]
r_n = []
for i in range(len(y_train)):
    r_n.append(np.random.normal(0, 4))
y_train = r_n + y_train


# %% functions
def phi(a, D):
    p = np.zeros(D + 1)
    for i in range(D + 1):
        p[i] = a ** i
    return p


def phi_matrix(X):
    p = np.zeros(shape=(D+1, len(X)))
    for index, x in enumerate(X):
        p[:, index] = phi(x, D)
    return p


def f_w(w, b):
    return np.dot(w, phi(b, D))


def error(w, c, y_train):
    E = 0
    for i in range(len(c)):
        E = E + (y_train[i] - f_w(w, c[i])) ** 2
    return E


# %% resulting weight vector w_s using LMS rule
w_s = np.zeros(D + 1)
iterations = 500
gamma = 0.001
for i in range(iterations):
    k = [x for x in range(7)] * iterations
    for j in k:
        e = (y_train[j] - np.dot(phi(x_train[j], D), w_s)) * phi(x_train[j], D)
        w_s = np.add(w_s, gamma * e)
e_w_s = error(w_s, x_train, y_train)
print(f"The approximated w* using the LMS rule is: {w_s}")
print(f"The training error here is: {e_w_s}")
# %% calculate optimal weight vector w_opt in closed form
phi_train = phi_matrix(x_train)
w_opt = np.matmul(np.linalg.inv(phi_train @ np.transpose(phi_train)) @ phi_train, np.transpose(y_train))
e_w_opt = error(w_opt, x_train, y_train)
print(f"The calculated w* using the closed form is: {w_opt}")
print(f"The training error here is: {e_w_opt}")
# %% plot resulting y with both w
plt.scatter(x_train, y_train, label="training data")
y_w_s = []
for i in range(len(x_train)):
    y_w_s.append(f_w(w_s, x_train[i]))
y_w_opt = []
for i in range(len(x_train)):
    y_w_opt.append(f_w(w_opt, x_train[i]))
plt.plot(x_train, y_w_s, label='w* approximated')
plt.plot(x_train, y_w_opt, label='w* closed form')
plt.legend()
plt.show()
# %% find optimal gamma by error and runtime
gammas = 1000
gamma = np.linspace(0.00001, 0.01, gammas)
w_g = np.zeros((gammas, D+1))
e_g = np.zeros(gammas)
times = np.zeros(gammas)
for i in range(gammas):
    w = np.zeros(D+1)
    random_x = [x for x in range(7)] * iterations
    start = time.time()
    for k in random_x:
        w = np.add(w, gamma[i] * (y_train[k] - np.dot(phi(x_train[k], D), w)) * phi(x_train[k], D))
        w_g[i] = w
    times[i] = time.time() - start
    e_g[i] = error(w_g[i], x_train, y_train)
best_gamma_by_error = gamma[e_g == np.amin(e_g)]
error_min = e_g[e_g == np.amin(e_g)]
best_gamma_by_time = gamma[times == np.amin(times)]
time_min = times[times == np.amin(times)]
plt.plot(gamma, e_g)
plt.scatter(best_gamma_by_error, error_min, marker='o')
plt.xlabel("Gamma")
plt.ylabel("Error")
plt.ylim(0, 250)
plt.show()
#plt.plot(gamma, times)
#plt.scatter(best_gamma_by_time, time_min, marker='o')
#plt.xlabel("Gamma")
#plt.ylabel("Runtime [s]")
# plt.ylim(0,250)
plt.show()
print(f"The best gamma by error is: {best_gamma_by_error}")


def gsi(x):
    img = np.ones(shape=(29, 29))
    m1 = np.random.normal(15, 2)
    m2 = np.random.normal(15, 2)
    for i in range(img[0].size):
        for j in range(img[1].size):
            if (((i - m1) ** 2 + (j - m2) ** 2 - (3 * x) ** 2) >= 0):
                img[i, j] = 0
            else:
                img[i, j] = 1
    return img.flatten()


def gsi_matrix(X):
    gsi_ = np.ones(shape=(29 * 29, len(X)))
    for index, x in enumerate(X):
        gsi_[:, index] = gsi(x)
    return gsi_


gsi_train = gsi_matrix(x_train)
y_train_gsi = 2 * x_train ** 2 - gsi_train + 1
w_opt_gsi_train = np.matmul(np.linalg.pinv(gsi_train @ np.transpose(gsi_train)) @ gsi_train, np.transpose(y_train_gsi))
y_hat_gsi_train = w_opt_gsi_train.T @ gsi_train


# plt.scatter(y_train_gsi,y_hat_gsi_train)
# plt.plot(gsi_train,y_train_gsi,label='y')
# plt.show()

def display_column_vectors(M, title=None, columns=10):
    """Dsiplays images stored as column vectors.

    Args:
        M (array)     : n_pixels x n_objects matrix storing the images in columns
        title (str)   : title of the plot
        columns (int) : how many columns in image
    """

    plt.figure(figsize=(8, 5))
    for i, image in enumerate(M.T):
        plt.subplot(len(M.T) // columns + 1, columns, 1 + i)
        plt.imshow(image.reshape(29, 29), cmap='gray')
        plt.xticks([])
        plt.yticks([])

    if title is not None:
        plt.suptitle(title)
    plt.show()


display_column_vectors(y_train_gsi, 'Training set')
display_column_vectors(y_hat_gsi_train, 'Prediction set')


def error123(y_hat, y):
    E = 0
    for i in range(len(y)):
        E = E + (y_hat[i] - y[i]) ** 2
    return E


e_w_gsi = error123(y_hat_gsi_train, y_train_gsi)

gsi_all = gsi_matrix(x)
y_gsi = 2 * x ** 2 - gsi_all + 1
w_opt_gsi = np.matmul(np.linalg.pinv(gsi_all @ np.transpose(gsi_all)) @ gsi_all, np.transpose(y_gsi))
y_hat_gsi_all = w_opt_gsi.T @ gsi_all
# plt.scatter(y_gsi,y_hat_gsi_all)
# #plt.plot(gsi_train,y_train_gsi,label='y')
# plt.show()
display_column_vectors(y_gsi, 'Training set')
display_column_vectors(y_hat_gsi_all, 'Prediction set')