import numpy as np
import matplotlib.pyplot as plt
import math
import random
import sklearn.datasets
from cvxopt import matrix
from cvxopt.blas import dot
from cvxopt import solvers

X,y = sklearn.datasets.make_blobs(n_samples=200, centers=2, n_features=2, center_box=(0, 10))
X[:,0]=X[:,0]/max(X[:,0])
X[:,1]=X[:,1]/max(X[:,1])
plt.plot(X[:, 0][y == 0], X[:, 1][y == 0], 'g^')
plt.plot(X[:, 0][y == 1], X[:, 1][y == 1], 'bs')
plt.show()

for i in range(y.shape[0]):
    if y[i]==0:
        y[i]=-1

red=X[np.where(y==1)]
blue=X[np.where(y==-1)]


#%% https://courses.csail.mit.edu/6.867/wiki/images/a/a7/Qp-cvxopt.pdf
#https://python.plainenglish.io/introducing-python-package-cvxopt-implementing-svm-from-scratch-dc40dda1da1f

def trainSVM(X, t, kernel=None,sigma=None, C=float('inf')):
    solvers.options['show_progress'] = False
    threshold = 1e-5
    t_resh= np.reshape(t,(len(t),1))

    if kernel is None:
        K = X @ X.T
    else:
        K = kernel(X, X, sigma)
    P = matrix(t_resh * K * t_resh.T)
    q = matrix(-1 * np.ones([len(t),1]))
    if C == float('inf'):
        G = matrix(np.diag(np.array([-1]*len(t)), 0) * 1.0)
        h = matrix(np.zeros([len(t), 1]))
    else:
        G1 = np.diag(np.array([-1]*len(t)), 0) * 1.0
        G2 = np.diag(np.array([1]*len(t)), 0) * 1.0
        G = matrix(np.concatenate((G1,G2),axis=0))
        h1 = np.zeros([len(t), 1])
        h2 = np.ones([len(t), 1]) * C * 1.0
        h = matrix(np.concatenate((h1,h2),axis=0))
    A = matrix(t.reshape(1, len(t)) * 1.0)
    b = matrix(0.0)
    sol = solvers.qp(P, q, G, h, A, b)
    
    data = [(i,val) for i, val in enumerate(sol['x']) if threshold < val]
    indices, alphas = zip(*data)
    Xs = [X[i] for i in indices]
    ts = [t[i] for i in indices]
    
    alpha = np.array(alphas)
    X = np.array(Xs)
    t = np.array(ts)

    if kernel is None:
        w = alpha * t @ X
        w_0 = t[0] - np.dot(w, X[0])
    else:
        for n in range(len(alpha)):
            if threshold < alpha[n] < C - threshold:
                sum_S = 0
                for m in range(len(alpha)):
                    sum_S = sum_S + alpha[m] * t[m] * old_rbfkernel(X[n], X[m], sigma)
                w_0 = t[n] - sum_S
                break

    return(alphas, X, t, w_0)


def discriminant(alpha, w0, X, t, Xnew, kernel=None):
    if kernel is None:
        K = X @ xNew.T
    else:
        K = kernel(X, xNew, sigma)
    return alpha * t @ K + np.ones(xNew.shape[0]) * w0

(alpha, X_sv, t_sv, w_0) = trainSVM(X,y)

w = alpha * t_sv @ X_sv
distance = 1 / np.linalg.norm(w)

plt.scatter(red[:,0], red[:,1], c='red', s=20)
plt.scatter(blue[:,0], blue[:,1], c='blue', s=20)
plt.plot([0,1], [-w_0/w[1],(-w_0-w[0])/w[1]], color='red')
plt.plot([distance, distance + 1], [-w_0/w[1],(-w_0-w[0])/w[1]], linestyle='--', color='blue')
plt.plot([-distance, -distance + 1], [-w_0/w[1],(-w_0-w[0])/w[1]], linestyle='--', color='red')
plt.scatter(X_sv[:,0], X_sv[:,1], edgecolors='blue', facecolors='none', s=250)

#plt.xlim(0, 12)
#plt.ylim(0, 12)
plt.show()


