import numpy as np
import matplotlib.pyplot as plt
import math
import random
from skimage.measure import label, regionprops
import mnist
import tensorflow as tf
from sklearn.linear_model import Perceptron
#%%

fashion_mnist = tf.keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

class_names = ['Trouser', 'Ankle boot']

train_subset_trouser = train_images[np.where(train_labels == 1)][:400]
train_subset_ankleboot = train_images[np.where(train_labels == 9)][:400]

#%%
#%%

def image_features(array_of_imgs):
    threshold=30
    feature_matrix=np.empty([array_of_imgs.shape[0],2])
    for i,img in enumerate(array_of_imgs):
        binary_image=1.0*(img>threshold)
        
        largest_area=0
        for region in regionprops(label(binary_image)):
            region_area=region.area
            if region_area>largest_area:
                largest_region=region
                kargest_area=region_area
        
        feature_matrix[i,0]=largest_region.eccentricity
        feature_matrix[i,1]=largest_region.perimeter
    return feature_matrix

features_ones=image_features(train_subset_trouser)
features_twos=image_features(train_subset_ankleboot)

plt.scatter(features_ones[:,0],features_ones[:,1])
plt.scatter(features_twos[:,0],features_twos[:,1])

#%%
def percTrain (X, t, maxIts=50, online=False):
    """
    Trains a perceptron on the given training data and returns the weight vector. Please note that the first component
    of the weight vector should be the homogeneous coordinate.
    X: Input vectors
    t: target values
    maxIts: Maximum number of iterations
    online: true if online optimization procedure should be used, false for batch-version
    """
    
    
    # augment X
    x_au = np.ones((X[:,0].size+1,X[0,:].size))
    for i in range(1, x_au[:,0].size):
        x_au[i] = X[i-1]

    w = np.zeros(X[:,0].size+1).T
    epoch = 0
    if(online):
        for e in range (1, maxIts-1):
            countEpoch = True
            for i in range(0,x_au[0,:].size):
                y_hat = np.sign(np.dot(w, x_au[:,i]))
                loss = max(0,-t[i]*(np.dot(w, x_au[:,i])))
                if(loss>0): 
                    w = w+(t[i]-y_hat)*x_au[:,i]
                    countEpoch = False
            if(countEpoch):
                epoch = e
                break
    else:
        for e in range (1, maxIts-1):
            countEpoch = True
            for i in range(0,x_au[0,:].size):
                if(np.dot(w, x_au[:,i]*t[i]) <= 0):
                    w = w+x_au[:,i]*t[i]
                    countEpoch = False
            if(countEpoch):
                epoch = e
                break
                
    return w

labels=np.concatenate((np.where(train_labels==1)[0][:400],np.where(train_labels==9)[0][:400]))
for i in range(labels.shape[0]):
    if train_labels[labels[i]]==9:
        labels[i]=-1
    else:
        labels[i]=1


X=np.concatenate((features_ones,features_twos)).T
w=percTrain(X,labels)
homogeneous_features_ones = np.concatenate((np.ones((400, 1)), features_ones), axis=1).T
homogeneous_features_twos = np.concatenate((np.ones((400, 1)), features_twos), axis=1).T
X=np.concatenate((homogeneous_features_ones,homogeneous_features_twos),axis=1)
def perc(weights,X):
    return np.sign(np.matmul(weights.T, X))
separable_ones = features_ones[np.where(perc(w, homogeneous_features_ones) < 0)][:100]
separable_twos = features_twos[np.where(perc(w, homogeneous_features_twos) < 0)][:100]

plt.scatter(separable_ones[:,0],separable_ones[:,1])
plt.scatter(separable_twos[:,0],separable_twos[:,1])

